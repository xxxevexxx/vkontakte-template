from .vkbottle_create_items import vkontakte_create_labeler
from .vkbottle_delete_items import vkontakte_delete_labeler
from .vkbottle_update_items import vkontakte_update_labeler

__all__ = (
    "vkontakte_create_labeler",
    "vkontakte_delete_labeler",
    "vkontakte_update_labeler"
)

labeleres = [
    vkontakte_create_labeler,
    vkontakte_delete_labeler,
    vkontakte_update_labeler
]