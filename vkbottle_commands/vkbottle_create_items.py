from vkbottle.dispatch.rules.base import CommandRule
from vkbottle.bot import BotLabeler, Message
from tortoise_models.basemodel import User


vkontakte_create_labeler = BotLabeler()


@vkontakte_create_labeler.message(CommandRule("create", ["/", "!"], 1))
async def vkontakte_create(message: Message, args):
    user_name = args[0]
    if await User.filter(user_name=user_name).first() is not None:
        return await message.answer("Такое имя уже используется.")
    else:
        await User.create(user_id=message.peer_id, user_name=user_name)
        return await message.answer("Новое имя создано успешно.")


#                          Пример диалога с ботом
#   USER: /create Ivan
#                                          Новое имя создано успешно. :BOT
#   USER: /create Ivan
#                                         Такое имя уже используется. :BOT