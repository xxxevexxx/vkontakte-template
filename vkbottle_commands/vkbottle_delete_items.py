from vkbottle.dispatch.rules.base import CommandRule
from vkbottle.bot import BotLabeler, Message
from tortoise_models.basemodel import User


vkontakte_delete_labeler = BotLabeler()


@vkontakte_delete_labeler.message(CommandRule("delete", ["/", "!"], 1))
async def vkontakte_delete(message: Message, args):
    user_name = args[0]
    if await User.filter(user_name=user_name).first() is None:
        return await message.answer("Данного имени нет в базе данных.")
    else:
        await User.filter(user_name=user_name).delete()
        return await message.answer("Имя успешно удалено из базы данных.")


#                          Пример диалога с ботом
#   USER: /delete Ivan
#                                 Имя успешно удалено из базы данных. :BOT
#   USER: /delete Ivan
#                                    Данного имени нет в базе данных. :BOT