from vkbottle.dispatch.rules.base import CommandRule
from vkbottle.bot import BotLabeler, Message
from tortoise_models.basemodel import User


vkontakte_update_labeler = BotLabeler()


@vkontakte_update_labeler.message(CommandRule("update", ["/", "!"], 2))
async def vkontakte_update(message: Message, args):
    old_user_name = args[0]
    new_user_name = args[1]
    if await User.filter(user_name=old_user_name).first() is None:
        return await message.answer("Данного имени нет в базе данных.")
    else:
        await User.filter(user_name=old_user_name).update(user_name=new_user_name)
        return await message.answer("Имя успешно изменено в базе данных.")


#                          Пример диалога с ботом
#   USER: /update Ivan Anton
#                                 Имя успешно изменено в базе данных. :BOT
#   USER: /update Ivan Anton
#                                    Данного имени нет в базе данных. :BOT