from tortoise import Model, fields


class User(Model):
    id = fields.IntField(pk=True)
    user_id = fields.IntField(max_length=255)
    user_name = fields.TextField(max_length=255, default="NoName")