# Vkontakte Template
**Vkontakte Template - это пример создания бота для Vkontakte, который использует библиотеку VKBottle для взаимодействия с VK API, фреймворк FastAPI для создания API и базу данных через Tortoise-ORM.**

## Используемые библиотеки
```
fastapi~=0.95.0
uvicorn~=0.21.1
vkbottle~=4.3.12
TgCrypto~=1.2.5
tortoise-orm~=0.19.3
requests~=2.28.2
loguru~=0.6.0
```

## Установка и запуск
1. **Устанавливаем GIT если не установлен.**

2. **Создаем новый проект в своем редакторе кода.**

3. **Клонируем проект:**
 > ```git clone https://gitlab.com/xxxevexxx/vkontakte-template.git```

4. **Устанавливаем зависимости:**
 > Poetry - `poetry install`
 > 
 > Venv - `pip install -r requirements.txt`

5. **Получаем ACCESS_TOKEN:**
 > https://vk.com/{yougroup}?act=tokens
 > 
 > Включаем Long Poll API, Типы событий ставим Входящее сообщение

6. **Корректируем файл config.py:**

```
ACCESS_TOKEN = "YourVkontakteToken"
```

7. **Запуск:**
 > Poetry - `poetry run main.py`
 > 
 > Venv - `python main.py`

### P.S Буду благодарен за любую обратную связь и ваши предложения по коду.
  
### Связь со мной:
- [x] [Telegram](https://t.me/xxxevexxx)
- [x] [LolzTeam](https://zelenka.guru/xxxevexxx)
- [x] [Vkontakte](https://vk.com/xxxevexxx)
