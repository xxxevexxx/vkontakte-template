from vkbottle import API, BuiltinStateDispenser
from vkbottle.bot import BotLabeler


class ProjectConfig:
    ACCESS_TOKEN = "YourVkontakteToken"
    MODEL_PATH = f"tortoise_models.basemodel"
    BASES_PATH = f"tortoise_bases/BasesMyProject"


api = API(ProjectConfig.ACCESS_TOKEN)
user = BotLabeler()
state_dispenser = BuiltinStateDispenser()