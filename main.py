import logging
import fastapi
import uvicorn

from loguru import logger
from vkbottle.bot import Bot
from tortoise import Tortoise
from config import ProjectConfig, api, user, state_dispenser
from vkbottle_commands import labeleres

from fastapi_response import fastapi_create_items, fastapi_delete_items, fastapi_update_items


logger.disable("vkbottle")


app = fastapi.FastAPI()
app.include_router(fastapi_create_items.router)
app.include_router(fastapi_delete_items.router)
app.include_router(fastapi_update_items.router)


@app.on_event("startup")
async def on_startup_tortoise():
    await Tortoise.init(
        db_url=f"sqlite://{ProjectConfig.BASES_PATH}",
        modules={"models": [ProjectConfig.MODEL_PATH]}
    )
    await Tortoise.generate_schemas()


@app.on_event("startup")
async def on_startup_vkbottle():
    for labeler in labeleres:
        user.load(labeler)
    await Bot(
        api=api,
        labeler=user,
        state_dispenser=state_dispenser,
    ).run_polling()


@app.on_event("shutdown")
async def on_shutdown_tortoise():
    await Tortoise.close_connections()


if __name__ == '__main__':
    uvicorn.run(app, host="127.0.0.1", port=80, log_level="debug")